//
//  CIPickerTests.m
//  CIPickerTests
//
//  Created by nguyen hula on 12/5/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "HomeViewModel.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "NimbusCore.h"
#import "NimbusModels.h"
#import "NimbusCollections.h"
#import "ContactModel.h"
#import "BaseViewController.h"
#import "ContactTableViewModel.h"
#import "ContactCollectionViewModel.h"
#import "ContactViewModel.h"
#import "GroupInfoViewModel.h"

@interface CIPickerTests : XCTestCase
@property (nonatomic, strong) ContactAPI* homeModel;
@end

@implementation CIPickerTests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.homeModel = [[ContactAPI alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    self.homeModel = nil;
}

- (void)testcheckPermissionForContacts {
//    XCTestExpectation * expection = [[XCTestExpectation alloc] initWithDescription:@"Handling completion for checking permission"];
    XCTAssertNoThrow([self.homeModel checkPermissionForContacts:^(NSError * _Nonnull error) {
//        [expection fulfill];
    }], @"No throw exception");
//    [self.homeModel checkPermissionForContacts:^(NSError * _Nonnull error) {
//        [expection fulfill];
//    }];
//    [self waitForExpectations:@[expection] timeout:10.0];
}

#pragma Test ContactTableViewModel

-(void) testInitWithContact {
    ContactTableViewModel * viewModel = [[ContactTableViewModel alloc] initWith:[NSArray new]];
    XCTAssertNotNil(viewModel);
}

-(void) testCreateModelWithDelegate {
    ContactTableViewModel * viewModel = [[ContactTableViewModel alloc] initWith:[NSArray new]];
    XCTAssertNoThrow([viewModel createModelWithDelegate:self]);
}

-(void) testFindObjectWithContact {
    ContactModel * model1 = [[ContactModel alloc] initWith:@"Alice Berk" softName:@"AB" phoneNumber:@[] avatar:nil];
    ContactModel * model2 = [[ContactModel alloc] initWith:@"Hula Nguyen" softName:@"HN" phoneNumber:@[] avatar:nil];
    ContactModel * model3 = [[ContactModel alloc] initWith:@"Ram Bam" softName:@"RB" phoneNumber:@[] avatar:nil];
    NSArray* array = @[model1, model2, model3];
    ContactTableViewModel * viewModel = [[ContactTableViewModel alloc] initWith:array];
    [viewModel createModelWithDelegate:self];
    ContactCellObject * result = [viewModel findObjectWithContact:model1];
    XCTAssertEqual(result.contact, model1);
}

-(void) testSearchdataWithText {
    ContactTableViewModel * viewModel = [[ContactTableViewModel alloc] initWith:[NSArray new]];
    XCTAssertNoThrow([viewModel searchdataWithText:@"" delegate:nil completion:^(NIMutableTableViewModel * _Nonnull model) {
        //Do nothing
    }]);
}

#pragma Test ContactCollectionViewModel

-(void) testInitWithContactCollection {
    ContactCollectionViewModel * viewModel = [[ContactCollectionViewModel alloc] initWithContact:[NSArray new]];
    XCTAssertNotNil(viewModel);
}

-(void) testCreateModelWithDelegateCollection {
    ContactCollectionViewModel * viewModel = [[ContactCollectionViewModel alloc] initWithContact:[NSArray new]];
    XCTAssertNoThrow([viewModel createModelWithDelegate:self]);
}

-(void) testRemoveObject {
    ContactModel * model1 = [[ContactModel alloc] initWith:@"Alice Berk" softName:@"AB" phoneNumber:@[] avatar:nil];
    ContactModel * model2 = [[ContactModel alloc] initWith:@"Hula Nguyen" softName:@"HN" phoneNumber:@[] avatar:nil];
    ContactModel * model3 = [[ContactModel alloc] initWith:@"Ram Bam" softName:@"RB" phoneNumber:@[] avatar:nil];
    NSArray* array = @[
                       [ContactCollectObject contactWithContact:model1],
                       [ContactCollectObject contactWithContact:model2],
                       [ContactCollectObject contactWithContact:model3],
                    ];
    ContactCollectionViewModel * viewModel = [[ContactCollectionViewModel alloc] initWithContact:array];
    [viewModel createModelWithDelegate:self];
    [viewModel removeObject:model1];
    XCTAssertEqual(2, [viewModel getCollectionContents].count);
}


-(void) testAddObjectForModel {
    ContactModel * model1 = [[ContactModel alloc] initWith:@"Alice Berk" softName:@"AB" phoneNumber:@[] avatar:nil];
    ContactModel * model2 = [[ContactModel alloc] initWith:@"Hula Nguyen" softName:@"HN" phoneNumber:@[] avatar:nil];
    ContactModel * model3 = [[ContactModel alloc] initWith:@"Ram Bam" softName:@"RB" phoneNumber:@[] avatar:nil];
    
    ContactCollectionViewModel * viewModel = [[ContactCollectionViewModel alloc] initWithContact:[NSArray new]];
    
    [viewModel addObjectForModel:model1];
    XCTAssertEqual(1, [viewModel getCollectionContents].count);
    [viewModel addObjectForModel:model2];
    XCTAssertEqual(2, [viewModel getCollectionContents].count);
    
    [viewModel addObjectForModel:model3];
    XCTAssertEqual(3, [viewModel getCollectionContents].count);
}

-(void) testGetCollectionContent {
    ContactCollectionViewModel * viewModel = [[ContactCollectionViewModel alloc] initWithContact:[NSArray new]];
    XCTAssertNotNil([viewModel getCollectionContents]);
}


-(void) testNumberOfSelectedContact {
    ContactModel * model1 = [[ContactModel alloc] initWith:@"Alice Berk" softName:@"AB" phoneNumber:@[] avatar:nil];
    ContactModel * model2 = [[ContactModel alloc] initWith:@"Hula Nguyen" softName:@"HN" phoneNumber:@[] avatar:nil];
    ContactModel * model3 = [[ContactModel alloc] initWith:@"Ram Bam" softName:@"RB" phoneNumber:@[] avatar:nil];
    NSArray* array = @[
                       [ContactCollectObject contactWithContact:model1],
                       [ContactCollectObject contactWithContact:model2],
                       [ContactCollectObject contactWithContact:model3],
                       ];
    ContactCollectionViewModel * viewModel = [[ContactCollectionViewModel alloc] initWithContact:array];
    
    XCTAssertEqual(3, [viewModel numberOfSelectedContact]);
}

#pragma Test ContactViewModel

-(void) testUpdateGrouContact {
    ContactModel * model1 = [[ContactModel alloc] initWith:@"Alice Berk" softName:@"AB" phoneNumber:@[] avatar:nil];
    ContactModel * model2 = [[ContactModel alloc] initWith:@"Hula Nguyen" softName:@"HN" phoneNumber:@[] avatar:nil];
    ContactModel * model3 = [[ContactModel alloc] initWith:@"Ram Bam" softName:@"RB" phoneNumber:@[] avatar:nil];
    NSArray* array = @[model1, model2, model3];
    ContactViewModel* viewModel = [[ContactViewModel alloc] init];
    [viewModel updateGrouContact:array];
    XCTAssertEqual(3, viewModel.groupOfContacts.count);
}

#pragma test GroupInfoViewModel
-(void) testGroupInfoModel {
    ContactModel * model1 = [[ContactModel alloc] initWith:@"Alice Berk" softName:@"AB" phoneNumber:@[] avatar:nil];
    ContactModel * model2 = [[ContactModel alloc] initWith:@"Hula Nguyen" softName:@"HN" phoneNumber:@[] avatar:nil];
    ContactModel * model3 = [[ContactModel alloc] initWith:@"Ram Bam" softName:@"RB" phoneNumber:@[] avatar:nil];
    NSArray* array = @[
                       [ContactCollectObject contactWithContact:model1],
                       [ContactCollectObject contactWithContact:model2],
                       [ContactCollectObject contactWithContact:model3],
                       ];
    GroupInfoViewModel* viewModel = [[GroupInfoViewModel alloc] init];
    [viewModel updateGrouContact:array];
    XCTAssertEqual(4, viewModel.collectionContents.count);
}
@end
