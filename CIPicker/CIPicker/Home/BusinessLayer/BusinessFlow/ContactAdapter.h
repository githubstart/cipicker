//
//  ContactAdapter.h
//  CIPicker
//
//  Created by LAP11984 on 12/11/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ContactAdapter : NSObject

-(void)checkPermission:(void (^)(NSArray* __nullable contactList))successBlock failure:(void (^)(NSError* __nullable error))failureBlock queue:(dispatch_queue_t) dispatchQueue;
//-(void)checkPermission:(void (^)(BOOL granted, ContactAuthStatus status, NSArray* __nullable contactList)) completionHandler queue:(dispatch_queue_t) dispatchQueue;
-(void)fetchContactList:(void (^)(NSArray* contactList, NSError* __nullable error)) completionHandler queue:(dispatch_queue_t) dispatchQueue;

@end

NS_ASSUME_NONNULL_END
