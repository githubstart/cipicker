//
//  ContactAdapter.m
//  CIPicker
//
//  Created by LAP11984 on 12/11/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import "ContactAdapter.h"
#import "ContactAPI.h"

@interface ContactAdapter ()

@property(nonatomic, strong) ContactAPI* contactAPI;
@property (nonatomic, strong) NSArray* arrayColorString;

@end

@implementation ContactAdapter

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.contactAPI = [[ContactAPI alloc] init];
        self.arrayColorString = @[@"#E4AAA7", @"#E8BE9D", @"#A4D0C4",@"#95C8DC"];
    }
    return self;
}

-(void)checkPermission:(void (^)(NSArray* __nullable contactList))successBlock failure:(void (^)(NSError* __nullable error))failureBlock queue:(dispatch_queue_t) dispatchQueue
{
    __weak typeof(self) weakSelf = self;
    [self.contactAPI checkPermission:^(BOOL granted, ContactAuthStatus status) {
        if (granted) {
            [weakSelf fetchContactList:^(NSArray * _Nonnull contactList, NSError * _Nullable error) {
                if (error || contactList == nil || contactList.count == 0) {
                    failureBlock(error);
                } else {
                    successBlock(contactList);
                }
            } queue:dispatchQueue];
        } else {
            NSError* error = [weakSelf getErrorWithStatus:status];
            failureBlock(error);
        }
    } queue:dispatchQueue];
}

- (void)fetchContactList:(void (^)(NSArray * _Nonnull, NSError * _Nullable))completionHandler queue:(dispatch_queue_t)dispatchQueue
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatchQueue, ^{
        [self.contactAPI fetchContactList:^(NSArray * _Nullable contactList, NSError * _Nullable error) {
            NSMutableArray* contactEntitiesArr = [NSMutableArray new];
            [contactList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [contactEntitiesArr addObject:[weakSelf createContactModel:obj]];
            }];
            completionHandler(contactEntitiesArr, error);
        } queue:dispatchQueue];
    });
}


-(NSError*) getErrorWithStatus:(ContactAuthStatus) status
{
    switch (status) {
        case ContactAuthStatusDenied: {
            NSError * error = [NSError errorWithDomain:@"Contact" code:100 userInfo: @{ NSLocalizedDescriptionKey: @"Contact list is denied access. Please go to Setting and enable it." }];
            return error;
            break;
        }
        case ContactAuthStatusRestricted: {
            NSError * error = [NSError errorWithDomain:@"Contact" code:100 userInfo: @{ NSLocalizedDescriptionKey: @"Access control have been retricted." }];
            return error;
            break;
        }
        default:
            return nil;
            break;
    }
}

- (ContactModel*) createContactModel:(CNContact*) contact
{
    NSString* fullName = [self getFullNameOfcontact:contact];
    NSString* softName = [self getSoftNameOfContact:contact];
    NSArray* phoneNumber = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
    UIImage * image = nil;
    NSString* backgroundColor = [self.arrayColorString objectAtIndex:arc4random() % self.arrayColorString.count];
    
    if (contact.thumbnailImageData) {
        image = [UIImage imageWithData:contact.thumbnailImageData];
    } else if (contact.imageData) {
        image = [UIImage imageWithData:contact.imageData];
    }
    
    return [[ContactModel alloc] initWith:fullName softName:softName phoneNumber:phoneNumber avatar:image backgroundColor:backgroundColor];
}

-(NSString*) getSoftNameOfContact:(CNContact*) contact
{
    NSString* softName = @"";
    NSString* giveName = [contact givenName];
    NSString* familyName = [contact familyName];
    if (familyName != nil && ![familyName isEqualToString:@""]) {
        softName = [NSString stringWithFormat:@"%@%@", [giveName substringToIndex:1], [familyName substringToIndex:1]];
    } else if (![giveName isEqualToString:@""]){
        softName = [NSString stringWithFormat:@"%@", [giveName substringToIndex:1]];
    }
    return softName;
}

-(NSString*) getFullNameOfcontact:(CNContact*) contact
{
    NSString* giveName = [contact givenName];
    NSString* familyName = [contact familyName];
    return [NSString stringWithFormat:@"%@ %@", giveName , familyName];
}

-(UIImage*) getImageFormContact:(CNContact*) contact
{
    return [UIImage imageWithData:[contact thumbnailImageData]];
}

@end
