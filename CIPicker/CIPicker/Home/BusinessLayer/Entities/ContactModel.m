//
//  ContactModel.m
//  CIPicker
//
//  Created by LAP11984 on 12/6/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import "ContactModel.h"

@interface ContactModel ()

@property(nonatomic, strong) NSString* fullName;
@property(nonatomic, strong) NSString* softName;
@property(nonatomic, strong) NSArray* phoneNumber;
@property(nonatomic, strong) UIImage* avatarImage;
@property(nonatomic, strong) NSString* backgroundColor;
@end

@implementation ContactModel

-(instancetype)initWith:(NSString*) fullName softName:(NSString*) softName phoneNumber:(NSArray*) phone avatar:(nullable UIImage*) image {
    
    if (self = [super init]) {
        self.fullName = fullName;
        self.softName = softName;
        self.phoneNumber = phone;
        self.avatarImage = image;
    }
    return self;
}

-(instancetype)initWith:(NSString*) fullName softName:(NSString*) softName phoneNumber:(NSArray*) phone avatar:(nullable UIImage*) image backgroundColor:(NSString*)backgroundColor {
    
    self = [self initWith:fullName softName:softName phoneNumber:phone avatar:image];
    self.backgroundColor = backgroundColor;
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    ContactModel* copy = [[[self class] allocWithZone:zone] init];
    
    if (copy) {
        // Copy NSObject subclasses
        copy.fullName = _fullName;
        copy.softName = _softName;
        copy.phoneNumber = _phoneNumber;
        copy.avatarImage = _avatarImage;
    }
    
    return copy;
}

@end
