//
//  ContactModel.h
//  CIPicker
//
//  Created by LAP11984 on 12/6/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, ContactAuthStatus)
{
    ContactAuthStatusNotDetermined = 0,
    ContactAuthStatusRestricted,
    ContactAuthStatusDenied,
    ContactAuthStatusAuthorized
} NS_ENUM_AVAILABLE(10_11, 9_0);

@interface ContactModel : NSObject <NSCopying>

@property(nonatomic, strong, readonly) NSString* fullName;
@property(nonatomic, strong, readonly) NSString* softName;
@property(nonatomic, strong, readonly) NSArray* phoneNumber;
@property(nonatomic, strong, readonly) UIImage* avatarImage;
@property(nonatomic, strong, readonly) NSString* backgroundColor;

-(instancetype)initWith:(NSString*) fullName softName:(NSString*) softName phoneNumber:(NSArray*) phone avatar:(nullable UIImage*) image;
-(instancetype)initWith:(NSString*) fullName softName:(NSString*) softName phoneNumber:(NSArray*) phone avatar:(nullable UIImage*) image backgroundColor:(NSString*)backgroundColor;
@end

NS_ASSUME_NONNULL_END
