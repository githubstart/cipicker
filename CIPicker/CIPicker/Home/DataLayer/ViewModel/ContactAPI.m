//
//  HomeViewModel.m
//  CIPicker
//
//  Created by nguyen hula on 12/5/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import "ContactAPI.h"

@interface ContactAPI ()

@end

@implementation ContactAPI

//-(void)checkPermission:(void (^)(BOOL grant, NSError* error)) completionHandler {
//    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
//    switch (status) {
//        case CNAuthorizationStatusDenied:
//        {
//            NSError * error = [NSError errorWithDomain:@"Contact" code:100 userInfo: @{ NSLocalizedDescriptionKey: @"Contact list is denied access. Please go to Setting and enable it." }];
//            completionHandler(false,error);
//            break;
//        }
//
//        case CNAuthorizationStatusNotDetermined:
//        case CNAuthorizationStatusAuthorized:
//            completionHandler(true, nil);
//            break;
//        default:
//        {
//            NSError * error = [NSError errorWithDomain:@"Contact" code:100 userInfo: @{ NSLocalizedDescriptionKey: @"Access control have been retricted." }];
//            completionHandler(false, error);
//            break;
//        }
//    }
//}

-(void)checkPermission:(void (^)(BOOL granted, ContactAuthStatus status)) completionHandler queue:(dispatch_queue_t) dispatchQueue {
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    switch (status) {
        case CNAuthorizationStatusDenied:
        {
            completionHandler(false, ContactAuthStatusDenied);
            break;
        }
        case CNAuthorizationStatusNotDetermined:
            completionHandler(true, ContactAuthStatusNotDetermined);
            break;
        case CNAuthorizationStatusAuthorized:
            completionHandler(true, ContactAuthStatusAuthorized);
            break;
        default:
        {
            completionHandler(false, ContactAuthStatusRestricted);
            break;
        }
    }
}


-(void)fetchContactList:(void (^)(NSArray* __nullable contactList, NSError* __nullable error)) completionHandler queue:(dispatch_queue_t) dispatchQueue; {
    
    CNContactStore *store = [[CNContactStore alloc] init];
    // contact should be fectched on background queue
    dispatch_async(dispatchQueue, ^{
        [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if (granted == YES) {
                //keys with fetching properties
                NSArray *keys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey,CNContactThumbnailImageDataKey];
                NSMutableArray* contactList = [[NSMutableArray alloc] init];
                CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];
                NSError *error;
                if( [store enumerateContactsWithFetchRequest:request error:&error usingBlock:^(CNContact * __nonnull contact, BOOL * __nonnull stop) {
                    if (error) {
                        NSLog(@"error fetching contacts %@", error);
                        //                    completionHandler(nil, error);
                    } else {
                        [contactList addObject:contact];
                    }
                }]) {
                    completionHandler(contactList,nil);
                }
            } else {
                completionHandler(nil, error);
            }
        }];
    });
}


-(CNContact*)getContactFormID:(NSString*)contactID {
    
    return [CNContact new];
}
@end
