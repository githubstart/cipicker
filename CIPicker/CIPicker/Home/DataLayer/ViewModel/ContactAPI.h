//
//  HomeViewModel.h
//  CIPicker
//
//  Created by nguyen hula on 12/5/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Contacts/Contacts.h>

NS_ASSUME_NONNULL_BEGIN

@interface ContactAPI : NSObject

-(void)checkPermission:(void (^)(BOOL granted, ContactAuthStatus status)) completionHandler queue:(dispatch_queue_t) dispatchQueue;
-(void)fetchContactList:(void (^)(NSArray* __nullable contactList, NSError* __nullable error)) completionHandler queue:(dispatch_queue_t) dispatchQueue;
-(CNContact*)getContactFormID:(NSString*)contactID;
@end

NS_ASSUME_NONNULL_END
