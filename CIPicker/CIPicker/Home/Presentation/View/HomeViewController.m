//
//  ViewController.m
//  CIPicker
//
//  Created by nguyen hula on 12/5/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import "HomeViewController.h"
#import "ContactViewController.h"
#import "ContactAdapter.h"

@interface HomeViewController ()

@property(nonatomic, strong) ContactAdapter* contactAdapter;
@property(weak, nonatomic) IBOutlet UIButton *createGroupBtn;
@property(weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property(nonatomic, strong) NSMutableArray* contactList;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Request permission and load contact
    
    self.contactAdapter = [[ContactAdapter alloc] init];
    self.contactList = [[NSMutableArray alloc] init];
   // setup init value for component
    self.createGroupBtn.alpha = 0.0;
    [self.loadingIndicator startAnimating];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    __weak typeof(self) weakSelf = self;
    [self.contactAdapter checkPermission:^(NSArray * _Nullable contactList) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.loadingIndicator stopAnimating];
            weakSelf.createGroupBtn.alpha = 1.0;
            [weakSelf.contactList addObjectsFromArray:contactList];
        });
    } failure:^( NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.loadingIndicator stopAnimating];
            [weakSelf showAlertWithMessage:error.localizedDescription];
        });
    } queue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
   
}

- (IBAction)didTapOnCreateGroup:(id)sender {
    UIStoryboard * mainStoryboad = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContactViewController * contactVC = [mainStoryboad instantiateViewControllerWithIdentifier:@"ContactViewController"];
    [contactVC updateViewModelWithData:self.contactList];
    [self.navigationController pushViewController:contactVC animated:true];
}

@end
