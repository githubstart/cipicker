//
//  ContactCollectionViewModel.h
//  CIPicker
//
//  Created by nguyen hula on 12/5/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactCollectionViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContactCollectionViewModel : NSObject

@property (nonatomic, strong) NICollectionViewModel* model;
-(instancetype)initWithContact:(NSArray*) contact;
-(void) createModelWithDelegate:(id<NICollectionViewModelDelegate>) delegate;
-(void) removeObject: (ContactModel*) object;
-(void) addObjectForModel: (ContactModel*) object;
-(NSArray*) getCollectionContents;
-(NSUInteger) numberOfSelectedContact;
@end

NS_ASSUME_NONNULL_END
