//
//  ContactCollectionViewModel.m
//  CIPicker
//
//  Created by nguyen hula on 12/5/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import "ContactCollectionViewModel.h"

@interface ContactCollectionViewModel ()

@property(nonatomic, strong)NSMutableArray* collectionContents;

@end

@implementation ContactCollectionViewModel

-(instancetype)initWithContact:(NSArray*) contact{
    if (self = [super init]) {
        self.collectionContents = [[NSMutableArray alloc] initWithArray:contact];
    }
    return self;
}

-(void) createModelWithDelegate:(id<NICollectionViewModelDelegate>) delegate {
    _model = [[NICollectionViewModel alloc] initWithListArray:self.collectionContents
                                                     delegate:delegate];
}

- (void)addObjectForModel:(ContactModel *)object {
    [self.collectionContents addObject:[ContactCollectObject contactWithContact:object]];
}

- (void)removeObject:(ContactModel *)object {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.contact == %@", object];
    NSArray* array = [self.collectionContents filteredArrayUsingPredicate:predicate];
    [self.collectionContents removeObjectsInArray:array];
}

-(NSArray*) getCollectionContents {
    return self.collectionContents;
}

-(NSUInteger) numberOfSelectedContact {
    return self.collectionContents.count;
}
@end
