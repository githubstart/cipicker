//
//  ContactCollectionViewController.h
//  CIPicker
//
//  Created by nguyen hula on 12/5/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactCollectionViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ContactCollectionViewControllerDelegate <NSObject>

-(void) scrollSelectedContactToTop:(ContactModel*) contact;

@end

@interface ContactCollectionViewController : UICollectionViewController

@property(weak, nonatomic) id<ContactCollectionViewControllerDelegate> delegate;

- (id)initWithCollectionViewLayout:(UICollectionViewLayout *)layout dataContact:(NSArray*) contact scrollDirection:(UICollectionViewScrollDirection) direction;
-(void) removeObject: (ContactModel*) object;
-(void) addObjectForModel: (ContactModel*) object;
// This method for getting array of ContactModel
-(NSArray*) getALLSelectedContact;
-(NSUInteger) numberOfSelectedContact;
// This method for get the array of ContactCollectObject
-(NSArray*) getCollectionOfSelectedContact;

@end

NS_ASSUME_NONNULL_END
