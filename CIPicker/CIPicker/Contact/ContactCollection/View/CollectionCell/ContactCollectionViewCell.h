//
//  ContactCollectionViewCell.h
//  CIPicker
//
//  Created by LAP11984 on 12/6/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ContactCollectObject : NSObject <NICollectionViewCellObject>

@property (nonatomic, strong) ContactModel * contact;
+ (instancetype)contactWithContact:(ContactModel *)contact;

@end

@interface ContactCollectionViewCell : UICollectionViewCell

@end

NS_ASSUME_NONNULL_END
