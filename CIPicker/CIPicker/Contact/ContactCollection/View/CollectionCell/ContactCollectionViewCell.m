//
//  ContactCollectionViewCell.m
//  CIPicker
//
//  Created by LAP11984 on 12/6/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import "ContactCollectionViewCell.h"

@implementation ContactCollectObject

+(instancetype)contactWithContact:(ContactModel *)contact {
    ContactCollectObject* instance = [[ContactCollectObject alloc] init];
    instance.contact = contact;
    return instance;
}

#pragma mark - NICollectionViewCellObject

- (UINib *)collectionViewCellNib {
    return [UINib nibWithNibName:NSStringFromClass([ContactCollectionViewCell class]) bundle:nil];
}


@end


@interface ContactCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation ContactCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (BOOL)shouldUpdateCellWithObject:(ContactCollectObject *)object {
   
    self.backgroundColor = [UIColor blueColor];
    if (object.contact.avatarImage) {
        self.textLabel.alpha = 0.0;
        self.imageView.image = object.contact.avatarImage;
    } else {
        self.textLabel.alpha = 1.0;
        if (object.contact.backgroundColor) {
            self.textLabel.backgroundColor = [UIColor colorFromHexString:object.contact.backgroundColor];
        } else {
            self.textLabel.backgroundColor = [UIColor lightGrayColor];
        }
        
        self.textLabel.text = object.contact.softName;
    }
    [self configCellView];
    return YES;
}


-(void) configCellView {
    self.layer.borderWidth=1.0;
    self.layer.masksToBounds = false;
    self.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.layer.cornerRadius = self.imageView.frame.size.height/2;
    self.clipsToBounds = true;
}
@end
