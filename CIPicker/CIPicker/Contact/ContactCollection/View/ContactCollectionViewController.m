//
//  ContactCollectionViewController.m
//  CIPicker
//
//  Created by nguyen hula on 12/5/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import "ContactCollectionViewController.h"

@interface ContactCollectionViewController ()<NICollectionViewModelDelegate>

@property(nonatomic, strong) ContactCollectionViewModel* contactCollecVM;

@end

@implementation ContactCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (id)initWithCollectionViewLayout:(UICollectionViewLayout *)layout dataContact:(NSArray*) contact scrollDirection:(UICollectionViewScrollDirection) direction{
    UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = direction;
    if ((self = [super initWithCollectionViewLayout:flowLayout])) {
        self.contactCollecVM = [[ContactCollectionViewModel alloc] initWithContact:contact];
        [self.contactCollecVM createModelWithDelegate:self];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
    self.collectionView.dataSource = self.contactCollecVM.model;
}

- (void) addObjectForModel: (ContactModel*) object {
    [self.contactCollecVM addObjectForModel:object];
    [self.collectionView reloadData];
}

- (void) removeObject: (ContactModel*) object {
    [self.contactCollecVM removeObject:object];

    [self.collectionView reloadData];
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ContactCollectObject * selectedContact = [self.contactCollecVM.model objectAtIndexPath:indexPath];
    if (self.delegate && [self.delegate respondsToSelector:@selector(scrollSelectedContactToTop:)])
    [self.delegate scrollSelectedContactToTop:selectedContact.contact];
}

- (UICollectionViewCell *)collectionViewModel:(NICollectionViewModel *)collectionViewModel cellForCollectionView:(UICollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath withObject:(id)object {
    return [NICollectionViewCellFactory collectionViewModel:collectionViewModel cellForCollectionView:collectionView atIndexPath:indexPath withObject:object];
}

-(NSArray*) getALLSelectedContact {
    
    NSArray* array =  [self.contactCollecVM getCollectionContents];
    NSMutableArray* contactModel = [[NSMutableArray alloc] init];
    
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [contactModel addObject:((ContactCollectObject*)obj).contact];
    }];
    return contactModel;
}

-(NSUInteger) numberOfSelectedContact {
    return [self.contactCollecVM numberOfSelectedContact];
}

-(NSArray*) getCollectionOfSelectedContact {
    return [self.contactCollecVM getCollectionContents];
}
@end
