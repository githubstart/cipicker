//
//  ContactTableViewModel.h
//  CIPicker
//
//  Created by nguyen hula on 12/5/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContactTableViewModel : NSObject

@property (nonatomic, strong) NIMutableTableViewModel* model;
@property (nonatomic, strong) NIMutableTableViewModel* searchModel;

-(instancetype)initWith:(NSArray*) contactData;
-(void) createModelWithDelegate:(id<NIMutableTableViewModelDelegate>) delegate;
-(ContactCellObject*) findObjectWithContact:(ContactModel*) contact;
- (void)searchdataWithText:(NSString *)searchtext delegate:(id<NIMutableTableViewModelDelegate>) delegate completion:(void(^)(NIMutableTableViewModel* model)) completion ;
@end

NS_ASSUME_NONNULL_END
