//
//  ContactTableViewModel.m
//  CIPicker
//
//  Created by nguyen hula on 12/5/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import "ContactTableViewModel.h"

@interface ContactTableViewModel ()

@property (nonatomic, strong) NSArray* sectionDataArray;
@property (nonatomic, strong) NSMutableArray* groupOfContact;
@property (nonatomic, strong) NSMutableArray * tableContents;
@property (nonatomic, strong) NSArray* originData;
@end

@implementation ContactTableViewModel

- (instancetype)initWith:(NSArray*) contactData
{
    self = [super init];
    if (self) {
        self.sectionDataArray = [[NSArray alloc] initWithObjects:@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",@"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z",@"#", nil];
        self.groupOfContact = [NSMutableArray arrayWithArray:contactData];
        self.tableContents = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void) createModelWithDelegate:(id<NIMutableTableViewModelDelegate>) delegate {
    
    [self.tableContents removeAllObjects];
    for (NSString *value in self.sectionDataArray) {
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"SELF.fullName beginswith[c] %@", value];
        NSArray* tmp = [self.groupOfContact filteredArrayUsingPredicate:predicate];
        if (tmp != nil && tmp.count > 0) {
            [self.tableContents addObject:value];
            [tmp enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [self.tableContents addObject:[ContactCellObject contactWithContact:obj serchPhone:nil]];
            }];
        }
    }
    self.originData = [[NSArray alloc] initWithArray:self.tableContents copyItems:false];
    _model = [[NIMutableTableViewModel alloc] initWithSectionedArray:self.tableContents delegate:delegate];
    [_model setSectionIndexType:NITableViewModelSectionIndexDynamic showsSearch:true showsSummary:true];
}

-(ContactCellObject*) findObjectWithContact:(ContactModel*) contact {
    
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF isKindOfClass:%@", [ContactCellObject class]];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"SELF.contact == %@", contact];
    NSPredicate* predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
    NSArray* array = [self.tableContents filteredArrayUsingPredicate:predicate];
    return array.firstObject;
}

- (void)searchdataWithText:(NSString *)searchtext delegate:(id<NIMutableTableViewModelDelegate>) delegate completion:(void(^)(NIMutableTableViewModel* model)) completion  {

    if (searchtext && ![searchtext isEqualToString:@""]) {
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF isKindOfClass:%@", [ContactCellObject class]];
        NSPredicate* predicate2 = nil;
        if ([self isStringContainOnlyNumber:searchtext]) {
            predicate2 = [NSPredicate predicateWithFormat:@"ANY SELF.contact.phoneNumber contains[c] %@",searchtext];
        } else {
            predicate2 = [NSPredicate predicateWithFormat:@"SELF.contact.fullName containsString:%@", searchtext];
        }
        
        NSPredicate* predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
        NSArray* array = [self repairDataForSearch:[self.originData filteredArrayUsingPredicate:predicate] searchText:searchtext];
        
        [self.tableContents removeAllObjects];
        [self.tableContents addObjectsFromArray:array];
        self.searchModel = [[NIMutableTableViewModel alloc] initWithListArray:array delegate:delegate];
        completion(self.searchModel);
    } else {
        [self.tableContents removeAllObjects];
        [self.tableContents addObjectsFromArray:self.originData];
        [self.tableContents enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isKindOfClass:[ContactCellObject class]]) {
                ((ContactCellObject*)obj).searchPhone = nil;
            }            
        }];
        completion(self.model);
    }
}

-(NSArray*) repairDataForSearch:(NSArray*) array searchText: (NSString*) searchText {
    NSArray * result = [[NSArray alloc] initWithArray:array copyItems:false];
    
     if ([self isStringContainOnlyNumber:searchText]) {
         [result enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSPredicate* predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",searchText];
             NSArray* array = [((ContactCellObject*)obj).contact.phoneNumber filteredArrayUsingPredicate:predicate];
             ((ContactCellObject*)obj).searchPhone = array.firstObject;
         }];
     } else {
         [result enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
             ((ContactCellObject*)obj).searchPhone = nil;
         }];
     }
    return result;
}

-(BOOL) isStringContainOnlyNumber:(NSString*) string {
    NSCharacterSet *_NumericOnly = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *myStringSet = [NSCharacterSet characterSetWithCharactersInString:string];
    
    if ([_NumericOnly isSupersetOfSet: myStringSet])
    {
        return true;
    }
    return false;
}
@end
