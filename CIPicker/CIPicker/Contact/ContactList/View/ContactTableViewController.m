//
//  ContactTableViewController.m
//  CIPicker
//
//  Created by nguyen hula on 12/5/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import "ContactTableViewController.h"
#import "ContactTableViewModel.h"
#import "ContactTableViewCell.h"

@interface ContactTableViewController ()<NIMutableTableViewModelDelegate, UISearchResultsUpdating>

@property(nonatomic, strong) ContactTableViewModel* tableViewModel;
@property(nonatomic, strong) UISearchController* searchController;
@property(nonatomic, strong) UITableView* tableView;
@property(nonatomic, strong) UIView *searchCotainer;
@end

@implementation ContactTableViewController

- (instancetype)initWithStyle:(UITableViewStyle)style contactData:(NSArray*) arrray
{
    self = [super init];
    if (self) {
        self.tableViewModel = [[ContactTableViewModel alloc] initWith:arrray];
        self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:style];
        self.searchCotainer = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpdateTableView];
    [self initSearchController];
    
    [self layoutForSearchBar];
    [self layoutForTableView];
    [self.view layoutIfNeeded];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.searchController dismissViewControllerAnimated:true completion:nil];
}

-(void) setUpdateTableView {
    self.tableView.backgroundColor = [UIColor whiteColor];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"MyCell"];

    [self.tableViewModel createModelWithDelegate:self];
    self.tableView.delegate = self;
    self.tableView.dataSource = self.tableViewModel.model;
    [self.tableViewModel.model updateSectionIndex];
    [self.tableView setEditing:YES animated:YES];
//    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
}

-(void) layoutForTableView {
    [self.view addSubview:self.tableView];
    self.tableView.translatesAutoresizingMaskIntoConstraints = false;
    [[self.tableView.topAnchor constraintEqualToAnchor:self.searchCotainer.bottomAnchor] setActive:true];
    [[self.tableView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor] setActive:true];
    [[self.tableView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor] setActive:true];
    [[self.tableView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor] setActive:true];
}

-(void) scrollSelectObjectToTop:(ContactModel*) contact {
    NIMutableTableViewModel* model = (self.tableViewModel.model == self.tableView.dataSource) ? self.tableViewModel.model : self.tableViewModel.searchModel;
    NSIndexPath * indexPath = [self getIndexPathFrom:model withContact:contact];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:true];
}

-(NSIndexPath*) getIndexPathFrom:(NIMutableTableViewModel*) model withContact:(ContactModel*) contact {
    ContactCellObject * contactObject = [self.tableViewModel findObjectWithContact:contact];
    NSIndexPath * indexPath = [model indexPathForObject:contactObject];
    return indexPath;
}
#pragma mark - UITableViewDelegate

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 3;
}

#pragma mark - NIMutableTableViewModelDelegate

- (BOOL)tableViewModel:(NIMutableTableViewModel *)tableViewModel
         canEditObject:(id)object
           atIndexPath:(NSIndexPath *)indexPath
           inTableView:(UITableView *)tableView {
    // We want every cell to be editable.
    return YES;
}

- (UITableViewCell *)tableViewModel:(NITableViewModel *)tableViewModel
                   cellForTableView:(UITableView *)tableView
                        atIndexPath:(NSIndexPath *)indexPath
                         withObject:(id)object {
    return [NICellFactory tableViewModel:tableViewModel cellForTableView:tableView atIndexPath:indexPath withObject:object];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NIMutableTableViewModel* model = (self.tableViewModel.model == self.tableView.dataSource) ? self.tableViewModel.model : self.tableViewModel.searchModel;
    id object = [model objectAtIndexPath:indexPath];
    if (_delegate && [_delegate respondsToSelector:@selector(didSelectedObject:)]) {
        [_delegate didSelectedObject:((ContactCellObject*)object).contact];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    NIMutableTableViewModel* model = (self.tableViewModel.model == self.tableView.dataSource) ? self.tableViewModel.model : self.tableViewModel.searchModel;
    id object = [model objectAtIndexPath:indexPath];
    if (_delegate && [_delegate respondsToSelector:@selector(didDeselectedObject:)]) {
        [_delegate didDeselectedObject:((ContactCellObject*)object).contact];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 18;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64;
}

#pragma search controller initilize

-(void) initSearchController {
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];;
    self.searchController.searchResultsUpdater = self;
    self.searchController.hidesNavigationBarDuringPresentation = false;
    self.searchController.dimsBackgroundDuringPresentation = false;
    self.searchController.searchBar.placeholder = @"Enter name, phone number";
    [self.searchController.searchBar setSearchBarStyle:UISearchBarStyleProminent];
}

-(void) layoutForSearchBar {
    [self.view addSubview:self.searchCotainer];
    self.searchCotainer.alpha = 1.0;
    self.searchCotainer.backgroundColor = [UIColor whiteColor];
    self.searchCotainer.translatesAutoresizingMaskIntoConstraints = false;
    [[self.searchCotainer.topAnchor constraintEqualToAnchor:self.view.topAnchor] setActive:true];
    [[self.searchCotainer.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor] setActive:true];
    [[self.searchCotainer.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor] setActive:true];
    [[self.searchCotainer.heightAnchor constraintEqualToConstant:56] setActive:true];
    [self.searchCotainer addSubview:self.searchController.searchBar];
}

#pragma implement protocol for UISearchResultsUpdating
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString* searchText = searchController.searchBar.text;
    __weak typeof(self) weakSelf = self;
    [self.tableViewModel searchdataWithText:searchText delegate:self completion:^(NIMutableTableViewModel * _Nonnull model) {
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.tableView.dataSource = model;
            [weakSelf.tableView reloadData];
            [weakSelf updateSelectedContactWithModel:model];
        });
    }];
}

-(void) updateSelectedContactWithModel : (NIMutableTableViewModel*) model{
    NSArray* array = nil;
    if (_delegate && [_delegate respondsToSelector:@selector(getAllSelectedContact)]) {
        array = [_delegate getAllSelectedContact];
        [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSIndexPath* indexPath =[self getIndexPathFrom:model withContact:(ContactModel *)obj];
                [self.tableView selectRowAtIndexPath:indexPath animated:false scrollPosition:UITableViewScrollPositionNone];
            });
        }];
    }
}

-(void) handleDeactiveAndActiveSearchControl {
    if (self.searchController.isActive) {
        [self.searchController setActive:false];
    }
}

-(void) dismissSearchBarControllerIfNeeded: (void (^ __nullable)(void))completion  {
    if ([[self.navigationController presentedViewController] isKindOfClass:[UISearchController class]]) {
        [self.searchController dismissViewControllerAnimated:true completion:completion];
    } else {
        completion();
    }
    
}
@end
