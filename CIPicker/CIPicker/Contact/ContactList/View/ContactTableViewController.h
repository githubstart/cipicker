//
//  ContactTableViewController.h
//  CIPicker
//
//  Created by nguyen hula on 12/5/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ContactTableViewControllerDelegate <NSObject>

//-(void) updateUIForTableView;
-(void) didSelectedObject:(ContactModel*) object;
-(void) didDeselectedObject:(ContactModel*) object;
-(NSArray*) getAllSelectedContact;
@end

@interface ContactTableViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, weak) id<ContactTableViewControllerDelegate> delegate;
-(instancetype)initWithStyle:(UITableViewStyle)style contactData:(NSArray*) arrray;
-(void) scrollSelectObjectToTop:(ContactModel*) contact;
-(void) handleDeactiveAndActiveSearchControl;
-(void) dismissSearchBarControllerIfNeeded: (void (^ __nullable)(void))completion;
@end

NS_ASSUME_NONNULL_END
