//
//  ContactTableViewCell.h
//  CIPicker
//
//  Created by nguyen hula on 12/5/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Contacts/Contacts.h>

NS_ASSUME_NONNULL_BEGIN

@interface ContactCellObject : NSObject <NSCopying, NICellObject>

@property (nonatomic, strong) ContactModel * contact;
@property (nonatomic, strong, nullable) NSString* searchPhone;
+ (instancetype)contactWithContact:(ContactModel *)contact serchPhone:(nullable NSString*)searchPhone;

@end

@interface ContactTableViewCell : UITableViewCell
@property(nonatomic, strong) UILabel* softNameLabel;
@end

NS_ASSUME_NONNULL_END
