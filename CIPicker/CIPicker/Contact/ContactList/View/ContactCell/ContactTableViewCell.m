//
//  ContactTableViewCell.m
//  CIPicker
//
//  Created by nguyen hula on 12/5/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import "ContactTableViewCell.h"

@implementation ContactCellObject

+ (instancetype)contactWithContact:(ContactModel *)contact serchPhone:(nullable NSString*)searchPhone {
    ContactCellObject* instance = [[ContactCellObject alloc] init];
    instance.contact = contact;
    instance.searchPhone = searchPhone;
    return instance;
}

- (id)copyWithZone:(NSZone *)zone
{
    ContactCellObject* copy = [[[self class] allocWithZone:zone] init];
    
    if (copy) {
        // Copy NSObject subclasses
        copy.contact = _contact;
        copy.searchPhone = _searchPhone;
    }
    
    return copy;
}

#pragma mark - ContactCellObject
- (Class)cellClass {
    return [ContactTableViewCell class];
}

@end

@implementation ContactTableViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {

    if (self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier]) {
        self.softNameLabel = [[UILabel alloc] init];
        self.softNameLabel.backgroundColor = [UIColor lightGrayColor];
        self.softNameLabel.textAlignment = NSTextAlignmentCenter;
        [self.softNameLabel setFont:[UIFont boldSystemFontOfSize:18]];
        [self.softNameLabel setTextColor:self.tintColor];
        [self.imageView addSubview:self.softNameLabel];
        self.softNameLabel.translatesAutoresizingMaskIntoConstraints = false;
        
        [[self.softNameLabel.topAnchor constraintEqualToAnchor:self.imageView.topAnchor] setActive:true];
        [[self.softNameLabel.bottomAnchor constraintEqualToAnchor:self.imageView.bottomAnchor] setActive:true];
        [[self.softNameLabel.leadingAnchor constraintEqualToAnchor:self.imageView.leadingAnchor] setActive:true];
        [[self.softNameLabel.trailingAnchor constraintEqualToAnchor:self.imageView.trailingAnchor] setActive:true];
    }
    return self;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (BOOL)shouldUpdateCellWithObject:(ContactCellObject *)object {

    self.textLabel.text = object.contact.fullName;
    self.detailTextLabel.text = object.searchPhone;
    if (object.contact.avatarImage) {
        self.softNameLabel.alpha = 0.0;
        self.imageView.image = object.contact.avatarImage;
    } else {
        self.imageView.image = [UIImage imageNamed:@"defaultAvatar"];
        self.softNameLabel.alpha = 1.0;
        self.softNameLabel.text = object.contact.softName;
        if (object.contact.backgroundColor) {
            self.softNameLabel.backgroundColor = [UIColor colorFromHexString:object.contact.backgroundColor];
        } else {
            self.softNameLabel.backgroundColor = [UIColor lightGrayColor];
        }
    }

    [self configImageView];
    
    return YES;
}

-(void) configImageView {
//    self.imageView.layer.borderWidth=1.0;
    self.imageView.layer.masksToBounds = false;
//    self.imageView.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.imageView.layer.cornerRadius = (self.imageView.frame.size.height/2 == 0) ? 32 : self.imageView.frame.size.height/2;
    self.imageView.clipsToBounds = true;
}

@end
