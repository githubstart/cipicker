//
//  ContactViewModel.m
//  CIPicker
//
//  Created by nguyen hula on 12/5/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import "ContactViewModel.h"

@interface ContactViewModel ()
@property (strong, nonatomic) NSMutableArray *groupOfContacts;
@end

@implementation ContactViewModel
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.groupOfContacts = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void) updateGrouContact:(NSArray*) contactData {
    [self.groupOfContacts removeAllObjects];
    [self.groupOfContacts addObjectsFromArray:contactData];
}
@end
