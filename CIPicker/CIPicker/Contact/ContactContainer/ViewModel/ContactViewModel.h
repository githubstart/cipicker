//
//  ContactViewModel.h
//  CIPicker
//
//  Created by nguyen hula on 12/5/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ContactViewModel : NSObject
@property (strong, nonatomic, readonly) NSMutableArray *groupOfContacts;
-(void) updateGrouContact:(NSArray*) contactData;
@end

NS_ASSUME_NONNULL_END
