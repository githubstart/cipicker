//
//  ContactViewController.m
//  CIPicker
//
//  Created by nguyen hula on 12/5/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import "ContactViewController.h"
#import "ContactViewModel.h"
#import "ContactTableViewController.h"
#import "ContactCollectionViewController.h"
#import "ContactCollectionViewCell.h"
#import "GroupInfoViewController.h"

@interface ContactViewController () <ContactTableViewControllerDelegate, ContactCollectionViewControllerDelegate>

@property(nonatomic, strong) ContactViewModel* contactVM;
@property(nonatomic, strong) ContactTableViewController* contactTableVC;
@property(nonatomic, strong) ContactCollectionViewController* contactCollectionVC;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *controlViewConstantHeigh;
@property (weak, nonatomic) IBOutlet UIView *tableContainerView;
@property (weak, nonatomic) IBOutlet UIView *controlView;
@property (weak, nonatomic) IBOutlet UIView *collectionContainerView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *selectedCountLabel;
@end

@implementation ContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.title = @"Contact";
//    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self initContactTable];
    [self initContactCollection];
    [self setUpForNavigationTitle];
}

#pragma setUp layout for navigition bar
-(void) setUpForNavigationTitle {
    [self createCustomNaviTitleView];
    [self createCustomLeftNavigationBarBtn];
    [self createCustomRighttNavigationBarBtn];
    [self updateNavigationTitle];
}

-(void) updateNavigationTitle {
    NSUInteger numberOfSelectedContact = [self.contactCollectionVC numberOfSelectedContact];
    self.selectedCountLabel.text = [NSString stringWithFormat:@"Selected : %ld", numberOfSelectedContact];
    [self updateLayoutAnimationWhenSeletedIfNeeded:numberOfSelectedContact];
    [self enableContinueNavigationBarIfNeeded: numberOfSelectedContact];
    [self.view layoutIfNeeded];
}

-(void) updateLayoutAnimationWhenSeletedIfNeeded:(NSUInteger) selectedContact {
    if ((selectedContact == 0 && self.controlViewConstantHeigh.constant != 0) || (selectedContact != 0  && self.controlViewConstantHeigh.constant == 0)){
        // Call this for deactive the search controller for fixing issue layout corrupt in case search controller show, and we layout for view.
        [self.contactTableVC handleDeactiveAndActiveSearchControl];
        
        [UIView animateWithDuration:0.3 animations:^{
            self.controlViewConstantHeigh.constant = (selectedContact > 0) ? 70 : 0;
           
        }];
    }
}

-(void) enableContinueNavigationBarIfNeeded:(NSUInteger) selectedContact {
    if (selectedContact == 0) {
        [self.navigationItem.rightBarButtonItem setEnabled:false];
        self.navigationItem.rightBarButtonItem.customView.alpha = 0.3;
    } else if (selectedContact == 1) {
        // TODO :UPdate Image sent for it
        [self.navigationItem.rightBarButtonItem setEnabled:true];
        self.navigationItem.rightBarButtonItem.customView.alpha = 1.0;
    } else {
        [self.navigationItem.rightBarButtonItem setEnabled:true];
        self.navigationItem.rightBarButtonItem.customView.alpha = 1.0;
    }
}

/// This method for create 2 lable for title view of navigation bar
-(void) createCustomNaviTitleView {
    UINavigationBar * navigationBar = self.navigationController.navigationBar;
    
    UIView *customView = [[UIView alloc] initWithFrame:navigationBar.bounds];;

    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.titleLabel.text = @"Create group";
    [self.titleLabel setTextColor:[UIColor blackColor]];
    [self.titleLabel setFont:[UIFont systemFontOfSize:17]];
    [self.titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    self.selectedCountLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.selectedCountLabel.text = @"Selected : 0";
    [self.selectedCountLabel setTextColor:[UIColor blackColor]];
    [self.selectedCountLabel setFont:[UIFont systemFontOfSize:12]];
    [self.selectedCountLabel setTextAlignment:NSTextAlignmentCenter];
    
    
    // create stack view for contain label
    UIStackView * stackView = [[UIStackView alloc] initWithArrangedSubviews:@[self.titleLabel, self.selectedCountLabel]];
    stackView.axis = UILayoutConstraintAxisVertical;
    stackView.distribution = UIStackViewDistributionEqualSpacing;
    stackView.alignment = UIStackViewAlignmentCenter;
    
    // Set up layout for stack view
    [customView addSubview:stackView];
    stackView.translatesAutoresizingMaskIntoConstraints = false;
    [stackView.topAnchor constraintEqualToAnchor:customView.topAnchor].active = true;
    [stackView.bottomAnchor constraintEqualToAnchor:customView.bottomAnchor].active = true;
    [stackView.leadingAnchor constraintEqualToAnchor:customView.leadingAnchor].active = true;
    [stackView.trailingAnchor constraintEqualToAnchor:customView.trailingAnchor].active = true;
    
    
    self.navigationItem.titleView = customView;
}


-(void) createCustomLeftNavigationBarBtn {
    self.navigationItem.hidesBackButton=YES;
    UIButton* customBtn = [self createCustomBarItem:@"Cancel"fontSize:15];
    
    [customBtn addTarget:self action:@selector(didSelectBackButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* someBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:customBtn];
    [self.navigationItem setLeftBarButtonItem:someBarButtonItem];
}

-(void) createCustomRighttNavigationBarBtn {
    self.navigationItem.hidesBackButton=YES;
    
    UIButton* customBtn = [self createCustomBarItem:@"Continue" fontSize:15];
    [customBtn addTarget:self action:@selector(didSelectContinueBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* customBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:customBtn];
    [self.navigationItem setRightBarButtonItem:customBarButtonItem];
}

#pragma Handle action when click on navigation bar
-(void) didSelectBackButton:(id) sender {
    __weak typeof(self) weakSelf = self;
    [self.contactTableVC dismissSearchBarControllerIfNeeded:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.navigationController popViewControllerAnimated:true];
        });
    }];
    
}

-(void) didSelectContinueBtn:(id) sender {
    __weak typeof(self) weakSelf = self;
    [self.contactTableVC dismissSearchBarControllerIfNeeded:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            UIStoryboard * mainStoryboad = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            GroupInfoViewController * groupInfoVc = [mainStoryboad instantiateViewControllerWithIdentifier:@"GroupInfoViewController"];
            [groupInfoVc updateViewModelWithData:[weakSelf.contactCollectionVC getCollectionOfSelectedContact]];
            [weakSelf.navigationController pushViewController:groupInfoVc animated:true];
        });
    }];
   
}

#pragma Init and update UI for Table View
-(void) initContactTable {
    self.contactTableVC = [[ContactTableViewController alloc] initWithStyle:UITableViewStylePlain contactData:self.contactVM.groupOfContacts];
    [self addChildViewController:self.contactTableVC];
    [self.contactTableVC didMoveToParentViewController:self];
    [self.tableContainerView addSubview:self.contactTableVC.view];
    [self addView:self.contactTableVC.view toSuperView:self.tableContainerView dictConst:@{
                                                                                           kTopConstant: @0,
                                                                                           kBottomConstant: @0,
                                                                                           kTraillingConstant: @0,
                                                                                           kleadingConstant: @0
                                                                                           }];
    self.contactTableVC.delegate = self;

}

#pragma Delegate for ContactTableViewController
- (void)didDeselectedObject:(ContactModel *)object {
    [self.contactCollectionVC removeObject:object];
    [self updateNavigationTitle];
}

-(NSArray*) getAllSelectedContact {
    NSArray* array =  [self.contactCollectionVC getALLSelectedContact];
    return array;
}


-(void)didSelectedObject:(ContactModel *)object {
    [self.contactCollectionVC addObjectForModel:object];
    [self updateNavigationTitle];
}

#pragma Init and update UI for Collection  View
-(void) initContactCollection {
    self.contactCollectionVC = [[ContactCollectionViewController alloc] initWithCollectionViewLayout: [UICollectionViewLayout new] dataContact:[NSArray new] scrollDirection:UICollectionViewScrollDirectionHorizontal];
    [self addChildViewController:self.contactCollectionVC];
    [self.contactCollectionVC didMoveToParentViewController:self];
    [self.collectionContainerView addSubview:self.contactCollectionVC.view];
    [self addView:self.contactCollectionVC.view toSuperView:self.collectionContainerView dictConst:@{
                                                                                                     kTopConstant: @00,
                                                                                                     kBottomConstant: @0,
                                                                                                     kTraillingConstant: @0,
                                                                                                     kleadingConstant: @0
                                                                                                     }];
    self.contactCollectionVC.delegate = self;
    [self.contactCollectionVC.collectionView setBackgroundColor:[UIColor colorFromHexString:@"#C9C9CE"]];
}

#pragma Delegate for ContactCollectionViewController
- (void)scrollSelectedContactToTop:(ContactModel *)contact {
    [self.contactTableVC scrollSelectObjectToTop:contact];
}

-(void) updateViewModelWithData:(NSArray*) contactData {
    if (self.contactVM == nil) {
        self.contactVM = [[ContactViewModel alloc] init];
    }
    [self.contactVM updateGrouContact:contactData];
}

@end
