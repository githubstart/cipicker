//
//  GroupInfoViewController.m
//  CIPicker
//
//  Created by nguyen hula on 12/9/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import "GroupInfoViewController.h"
#import "GroupInfoViewModel.h"
#import "ContactCollectionViewController.h"

@interface GroupInfoViewController ()<UITextFieldDelegate>

@property(nonatomic, strong) GroupInfoViewModel* groupInfoViewModel;
@property(nonatomic, strong) ContactCollectionViewController* contactCollectionVC;

@property (weak, nonatomic) IBOutlet UIView *collectionViewContainer;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UITextField *groupNameTextField;

@end

@implementation GroupInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Group Information";
    [self initContactCollection];
    [self createCustomRighttNavigationBarBtn];
    self.infoLabel.text = [NSString stringWithFormat:@"Total members of group : %lu", [self.contactCollectionVC numberOfSelectedContact]];
    [self.groupNameTextField becomeFirstResponder];
}

-(void) updateViewModelWithData:(NSArray*) contactData {
    if (self.groupInfoViewModel == nil) {
        self.groupInfoViewModel = [[GroupInfoViewModel alloc] init];
    }
    [self.groupInfoViewModel updateGrouContact:contactData];
}


-(void) initContactCollection {
    self.contactCollectionVC = [[ContactCollectionViewController alloc] initWithCollectionViewLayout: [UICollectionViewLayout new] dataContact:self.groupInfoViewModel.collectionContents scrollDirection:UICollectionViewScrollDirectionVertical];
    [self addChildViewController:self.contactCollectionVC];
    [self.contactCollectionVC didMoveToParentViewController:self];
    [self.collectionViewContainer addSubview:self.contactCollectionVC.view];
    [self addView:self.contactCollectionVC.view toSuperView:self.collectionViewContainer dictConst:@{
                                                                                                     kTopConstant: @30,
                                                                                                     kBottomConstant: @0,
                                                                                                     kTraillingConstant: @0,
                                                                                                     kleadingConstant: @0
                                                                                                     }];
}


-(void) createCustomRighttNavigationBarBtn {
    UIButton* customBtn = [self createCustomBarItem:@""fontSize:15];
    [customBtn setImage:[UIImage imageNamed:@"sentIcon"] forState:UIControlStateNormal];
    [customBtn addTarget:self action:@selector(handleCreateGroupAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* customBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:customBtn];
    [self.navigationItem setRightBarButtonItem:customBarButtonItem];

}


-(void) handleCreateGroupAction: (id) sender {
    [self showAlertWithMessage:[NSString stringWithFormat:@"Create group name %@ successful!", self.groupNameTextField.text]];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}

#pragma UITextField delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

// It is important for you to hide the keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
