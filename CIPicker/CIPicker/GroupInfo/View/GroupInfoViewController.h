//
//  GroupInfoViewController.h
//  CIPicker
//
//  Created by nguyen hula on 12/9/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GroupInfoViewController : BaseViewController
-(void) updateViewModelWithData:(NSArray*) contactData ;
@end

NS_ASSUME_NONNULL_END
