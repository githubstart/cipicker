//
//  GroupInfoViewModel.h
//  CIPicker
//
//  Created by nguyen hula on 12/9/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GroupInfoViewModel : NSObject
@property(nonatomic, strong)NSMutableArray* collectionContents;
-(void) updateGrouContact:(NSArray*) contactData;
@end

NS_ASSUME_NONNULL_END
