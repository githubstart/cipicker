//
//  GroupInfoViewModel.m
//  CIPicker
//
//  Created by nguyen hula on 12/9/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import "GroupInfoViewModel.h"
#import "ContactCollectionViewCell.h"

@implementation GroupInfoViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.collectionContents = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void) updateGrouContact:(NSArray*) contactData {
    [self.collectionContents removeAllObjects];
    //Add default data in contact group.
    ContactModel* object = [[ContactModel alloc] initWith:@"Default" softName:@"ME" phoneNumber:[NSArray new] avatar:[UIImage imageNamed:@"defaultAvatar"]];
    [self.collectionContents addObject:[ContactCollectObject contactWithContact:object]];
    [self.collectionContents addObjectsFromArray:contactData];
}
@end
