//
//  AppDelegate.h
//  CIPicker
//
//  Created by nguyen hula on 12/5/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

