//
//  BaseViewController.m
//  CIPicker
//
//  Created by nguyen hula on 12/9/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import "BaseViewController.h"

const NSString* kTopConstant = @"topConstant";
const NSString* kBottomConstant = @"bottomConstant";
const NSString* kleadingConstant = @"leadingConstant";
const NSString* kTraillingConstant = @"traillingConstant";

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


-(UIButton*) createCustomBarItem:(NSString*) title fontSize:(CGFloat) size{
    UIButton* customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [customBtn setTitle:title forState:UIControlStateNormal];
    [customBtn setTitleColor:customBtn.tintColor forState:UIControlStateNormal];
    [customBtn setShowsTouchWhenHighlighted:YES];
    [customBtn.titleLabel setFont:[UIFont systemFontOfSize:size]];
    return customBtn;
}

-(void) addView:(UIView*) view toSuperView:(UIView*) superView dictConst:(NSDictionary*) dict{
    view.translatesAutoresizingMaskIntoConstraints = false;
    
    [[view.topAnchor constraintEqualToAnchor:superView.topAnchor constant:[[dict objectForKey:kTopConstant] floatValue]] setActive:true];
    [[view.bottomAnchor constraintEqualToAnchor:superView.bottomAnchor constant:[[dict objectForKey:kBottomConstant] floatValue]] setActive:true];
    [[view.leadingAnchor constraintEqualToAnchor:superView.leadingAnchor constant:[[dict objectForKey:kleadingConstant] floatValue]] setActive:true];
    [[view.trailingAnchor constraintEqualToAnchor:superView.trailingAnchor constant:[[dict objectForKey:kTraillingConstant] floatValue]] setActive:true];
}

-(void) showAlertWithMessage:(NSString*) message {
    UIAlertController * alertVC = [UIAlertController alertControllerWithTitle:@"CIPicker" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    [alertVC addAction:action];
    [self presentViewController:alertVC animated:true completion:nil];
}
@end
