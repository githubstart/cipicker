//
//  UIColor+Extention.h
//  CIPicker
//
//  Created by LAP11984 on 12/10/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (Extention)
+ (UIColor *)colorFromHexString:(NSString *)hexString ;
@end

NS_ASSUME_NONNULL_END
