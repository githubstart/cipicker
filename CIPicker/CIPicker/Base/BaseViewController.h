//
//  BaseViewController.h
//  CIPicker
//
//  Created by nguyen hula on 12/9/18.
//  Copyright © 2018 nguyen hula. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

extern const NSString* kTopConstant;
extern const NSString* kBottomConstant ;
extern const NSString* kleadingConstant ;
extern const NSString* kTraillingConstant ;

@interface BaseViewController : UIViewController

-(UIButton*) createCustomBarItem:(NSString*) title fontSize:(CGFloat) size;
-(void) addView:(UIView*) view toSuperView:(UIView*) superView dictConst:(NSDictionary*) dict;
-(void) showAlertWithMessage:(NSString*) message;
@end

NS_ASSUME_NONNULL_END
